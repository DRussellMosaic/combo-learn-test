import React, { useState } from 'react';
import { NavBar, Nav, NavRight } from 'aws-amplify-react';
import { AmplifySignOut} from '@aws-amplify/ui-react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import mlLogo from '../images/MOSAIC_Learning-logo-high.png';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
}));

function  TitleBar({username})  {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const classes = useStyles();
    const menuId = 'primary-search-account-menu';
    const isMenuOpen = Boolean(anchorEl);

    const handleProfileMenuOpen = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => {
     setAnchorEl(null);
    };

    const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}><AmplifySignOut /></MenuItem>
    </Menu>
  );

    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <img src={mlLogo} alt="Mosaic Learning" height="42" width="150"/>
            </Typography>

            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              {username}
            </IconButton>

          </Toolbar>
        </AppBar>
        {renderMenu}
      </div>
    );
}

export default TitleBar;
