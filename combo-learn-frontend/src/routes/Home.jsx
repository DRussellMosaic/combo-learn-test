import React from 'react';
import Container from '@material-ui/core/Container';
import CourseList from '../components/CourseList';
import { gql } from "apollo-boost";
import { useQuery } from '@apollo/react-hooks';
import '../App.css';

const COURSES_AVAILABLE = gql`
  query {
    courses {
      fullname
      summary
      courseimage
      coursecategory
    }
  }
`;

function Home() {
   const { loading, error, data } = useQuery(COURSES_AVAILABLE);

   if (loading) return <p>Loading...</p>;
   if (error) return <p>Error :(</p>;

  return (
    <Container maxWidth="lg">
      <h2 className="availableheader">Available Courses</h2>
      <CourseList availableCourses={data} />
    </Container>
  );
}

export default Home;
