(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/handler.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/graphql/dataSources/character.js":
/*!**********************************************!*\
  !*** ./src/graphql/dataSources/character.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);\n // such kind of memoization is not good for production\n// use it like this only for demo purposes,\n// normally it is better to have something\n// like Redis here\n\nconst cache = {};\n\nconst extractId = url => {\n  const found = url.match(/(\\d+)\\/$/);\n\n  if (found.length) {\n    return found[1];\n  }\n\n  return null;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (async ids => {\n  if (!ids || !ids.length) {\n    return [];\n  }\n\n  const result = [];\n  const missing = [];\n  ids.forEach(id => {\n    // check what we already have in the cache\n    if (cache[id]) {\n      result.push(cache[id]);\n    } else {\n      missing.push(id);\n    }\n  });\n\n  if (missing.length) {\n    // still having cache miss? request then!\n    // console.log('in api call start');\n    let promises = await Promise.all(missing.map(id => {\n      return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`https://swapi.py4e.com/api/people/${id}/`).catch(error => console.log('api error', error));\n    }));\n    promises.forEach(res => {\n      // process the result as it does not\n      // have an appropriate format\n      // console.log('in api call response', res);\n      if (res) {\n        const data = res.data;\n        const id = extractId(data.url); // console.log('id', id);\n\n        if (id) {\n          const character = {\n            id,\n            fullName: data.name,\n            movies: data.films.map(filmURL => extractId(filmURL))\n          }; // put to the cache\n\n          cache[character.id] = character; // and to the result\n\n          result.push(character);\n        }\n      }\n    });\n  }\n\n  return result.filter(x => !!x);\n});\n\n//# sourceURL=webpack:///./src/graphql/dataSources/character.js?");

/***/ }),

/***/ "./src/graphql/dataSources/course.js":
/*!*******************************************!*\
  !*** ./src/graphql/dataSources/course.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst courses = [{\n  id: 59,\n  fullname: 'Celebrating Cultures',\n  shortname: 'Celebrating Cultures',\n  idnumber: '',\n  summary: '<p>Mount Orange is proud to have in its community students, teachers, parents and helpers from a wide variety of different cultures. This course, open to anyone, showcases the diversity of our traditions, our language and our landscapes. Please join in<img class=\"icon emoticon\" alt=\"smile\" title=\"smile\" src=\"https://school.moodledemo.net/theme/image.php/boost/core/1588010456/s/smiley\" /></p> <p><span class=\"label label-default\">Ages 8-adult</span></p>',\n  summaryformat: 1,\n  startdate: 1387407600,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Celebrating Cultures',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=59',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/1033/course/overviewfiles/diversity.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: true,\n  hidden: false,\n  timeaccess: 1588015859,\n  showshortname: false,\n  coursecategory: 'Society and Environment'\n}, {\n  id: 66,\n  fullname: 'Digital Literacy ',\n  shortname: 'Digital Literacy',\n  idnumber: '',\n  summary: '<p>Introducing the concept of Digital Literacy. Optimised for mobile.</p>',\n  summaryformat: 1,\n  startdate: 1487545200,\n  enddate: 1577314800,\n  visible: true,\n  fullnamedisplay: 'Digital Literacy ',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=66',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/1313/course/overviewfiles/digitalliteracy.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588015605,\n  showshortname: false,\n  coursecategory: 'Art and Media'\n}, {\n  id: 6,\n  fullname: 'Class and Conflict in World Cinema',\n  shortname: 'Cinema',\n  idnumber: '',\n  summary: '<p><span style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\">In this module we will analyse two very significant films -</span><span class=\"nolink\" style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\"> City of God</span><span style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\"> and </span><span class=\"nolink\" style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\">La Haine</span><span style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\">, both of which depict violent lives in poor conditions, the former in the favelas of Brazil and the latter in a Parisian banlieue. We will look at how conflict and class are portrayed, focusing particularly on the use of mise en scène.</span><br /></p><p><span style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\"><br /></span></p><p><span style=\"color:rgb(51,51,102);font-size:13.63636302948px;line-height:normal;\"><br /></span></p>\\r\\n<p><span class=\"label label-default\">Ages 16+</span></p>\\r\\n\\r\\n<p><br /></p>',\n  summaryformat: 1,\n  startdate: 1544828400,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Class and Conflict in World Cinema',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=6',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/43/course/overviewfiles/cinema.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588015295,\n  showshortname: false,\n  coursecategory: 'Art and Media'\n}, {\n  id: 64,\n  fullname: 'New staff induction',\n  shortname: 'Induction',\n  idnumber: '',\n  summary: '',\n  summaryformat: 1,\n  startdate: 1464044400,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'New staff induction',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=64',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/1271/course/overviewfiles/workplaceinduction.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588014755,\n  showshortname: false,\n  coursecategory: 'Mount Orange Community'\n}, {\n  id: 41,\n  fullname: 'Staffroom',\n  shortname: 'Staffroom',\n  idnumber: '',\n  summary: '<p>A space for staff only - not normally accessible to students.</p>\\r\\n<p>Important professional news, updates, documents as well as social updates and events.</p>',\n  summaryformat: 1,\n  startdate: 1275433200,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Staffroom',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=41',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/578/course/overviewfiles/staffroom.jpg',\n  progress: 0,\n  hasprogress: true,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588014722,\n  showshortname: false,\n  coursecategory: 'Mount Orange Community'\n}, {\n  id: 27,\n  fullname: 'Parents and Citizens Council',\n  shortname: 'Parents and Citizens Council',\n  idnumber: '',\n  summary: '<p>A space for members of Parents and Citizens council, an important part of our school community.</p>',\n  summaryformat: 1,\n  startdate: 1400083200,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Parents and Citizens Council',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=27',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/269/course/overviewfiles/parentscitizens.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588014719,\n  showshortname: false,\n  coursecategory: 'Mount Orange Community'\n}, {\n  id: 62,\n  fullname: 'Psychology in Cinema',\n  shortname: 'Psych Cine',\n  idnumber: '',\n  summary: '<p>In this course we study three films: Spider, A Beautiful Mind, and Fight Club. The main focus of the course will be the ways in which psychosis is represented in the films in terms of macro, plot, narrative structure and micro etc. We consider the wider cultural meaning and implication of films dealing with psychology.</p><p><span class=\"label label-default\">Ages 16+</span> </p>\\r\\n\\r\\n<p><br /></p>',\n  summaryformat: 1,\n  startdate: 1405551600,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Psychology in Cinema',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=62',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/1107/course/overviewfiles/psychocine.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588014632,\n  showshortname: false,\n  coursecategory: 'Art and Media'\n}, {\n  id: 38,\n  fullname: 'Moodle History Quiz',\n  shortname: 'Quiz',\n  idnumber: '',\n  summary: '<p>This is an example of the Single activity course format. It only displays one activity, which in this case is a Quiz, allowing the teacher to focus students\\' attention just on one learning area. Log in with username <b>parent </b>and password <b>moodle </b> to see it in action.<br /></p>',\n  summaryformat: 1,\n  startdate: 1400194800,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'Moodle History Quiz',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=38',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/545/course/overviewfiles/quiz-2137664_640.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1588014162,\n  showshortname: false,\n  coursecategory: 'Moodle Resources'\n}, {\n  id: 56,\n  fullname: 'History: Russia in Revolution',\n  shortname: 'History: Russia Rev',\n  idnumber: '',\n  summary: '<p>This course is designed for students studying Russian history. While it is aimed at British students working towards an examination, the content is suitable for any class studying the subject.</p><p><span class=\"label label-default\">Ages 15-18</span></p>\\r\\n\\r\\n\\r\\n<p><br /></p>',\n  summaryformat: 1,\n  startdate: 1385074800,\n  enddate: 0,\n  visible: true,\n  fullnamedisplay: 'History: Russia in Revolution',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=56',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/934/course/overviewfiles/russia.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1587745759,\n  showshortname: false,\n  coursecategory: 'Society and Environment'\n}, {\n  id: 51,\n  fullname: ' Moodle and Mountaineering',\n  shortname: 'Moodle Mountain',\n  idnumber: '',\n  summary: '<p>This course is for senior students planning an ascent of Mont Blanc in July. It is also designed to take Moodle newbies through a number of activities showing off the best of Moodle.</p>\\r\\n<p><span class=\"label label-default\">Ages 16+</span></p>\\r\\n\\r\\n\\r\\n<p><br /></p>',\n  summaryformat: 1,\n  startdate: 1399330800,\n  enddate: 1495753200,\n  visible: true,\n  fullnamedisplay: ' Moodle and Mountaineering',\n  viewurl: 'https://school.moodledemo.net/course/view.php?id=51',\n  courseimage: 'https://school.moodledemo.net/pluginfile.php/777/course/overviewfiles/MontBlanc.jpg',\n  progress: 0,\n  hasprogress: false,\n  isfavourite: false,\n  hidden: false,\n  timeaccess: 1529407699,\n  showshortname: false,\n  coursecategory: 'Physical Education'\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (ids => {\n  if (ids === null) {\n    return courses;\n  }\n\n  return courses.filter(course => ids.indexOf(course.id) >= 0);\n});\n\n//# sourceURL=webpack:///./src/graphql/dataSources/course.js?");

/***/ }),

/***/ "./src/graphql/dataSources/movie.js":
/*!******************************************!*\
  !*** ./src/graphql/dataSources/movie.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst movies = [{\n  id: '1',\n  title: 'A New Hope',\n  characters: [1, 2, 3, 4, 5]\n}, {\n  id: '2',\n  title: 'The Empire Strikes Back',\n  characters: [1, 2, 3]\n}, {\n  id: '3',\n  title: 'Return of the Jedi',\n  characters: [30, 31, 45]\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (ids => {\n  if (ids === null) {\n    return movies;\n  }\n\n  return movies.filter(movie => ids.indexOf(movie.id) >= 0);\n});\n\n//# sourceURL=webpack:///./src/graphql/dataSources/movie.js?");

/***/ }),

/***/ "./src/graphql/resolvers/character.js":
/*!********************************************!*\
  !*** ./src/graphql/resolvers/character.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  Character: {\n    movies: async (source, args, {\n      dataSources\n    }, state) => {\n      return dataSources.movieSource(source.movies);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/graphql/resolvers/character.js?");

/***/ }),

/***/ "./src/graphql/resolvers/course.js":
/*!*****************************************!*\
  !*** ./src/graphql/resolvers/course.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  Query: {\n    courses: async (source, args, {\n      dataSources\n    }, state) => {\n      return dataSources.courseSource(null);\n    },\n    course: async (source, args, {\n      dataSources\n    }, state) => {\n      // by using \"args\" argument we can get access\n      // to query arguments\n      const {\n        id\n      } = args;\n      const result = dataSources.courseSource([id]);\n\n      if (result && result[0]) {\n        return result[0];\n      }\n\n      return null;\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/graphql/resolvers/course.js?");

/***/ }),

/***/ "./src/graphql/resolvers/index.js":
/*!****************************************!*\
  !*** ./src/graphql/resolvers/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! merge-graphql-schemas */ \"merge-graphql-schemas\");\n/* harmony import */ var merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _movie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movie */ \"./src/graphql/resolvers/movie.js\");\n/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./character */ \"./src/graphql/resolvers/character.js\");\n/* harmony import */ var _course__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./course */ \"./src/graphql/resolvers/course.js\");\n\n\n\n\nconst resolvers = [_course__WEBPACK_IMPORTED_MODULE_3__[\"default\"], _movie__WEBPACK_IMPORTED_MODULE_1__[\"default\"], _character__WEBPACK_IMPORTED_MODULE_2__[\"default\"]];\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__[\"mergeResolvers\"])(resolvers));\n\n//# sourceURL=webpack:///./src/graphql/resolvers/index.js?");

/***/ }),

/***/ "./src/graphql/resolvers/movie.js":
/*!****************************************!*\
  !*** ./src/graphql/resolvers/movie.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  Query: {\n    movies: async (source, args, {\n      dataSources\n    }, state) => {\n      return dataSources.movieSource(null);\n    },\n    movie: async (source, args, {\n      dataSources\n    }, state) => {\n      // by using \"args\" argument we can get access\n      // to query arguments\n      const {\n        id\n      } = args;\n      const result = dataSources.movieSource([id]);\n\n      if (result && result[0]) {\n        return result[0];\n      }\n\n      return null;\n    }\n  },\n  Movie: {\n    characters: async (source, args, {\n      dataSources\n    }) => {\n      return dataSources.characterSource(source.characters);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/graphql/resolvers/movie.js?");

/***/ }),

/***/ "./src/graphql/types/character.graphql":
/*!*********************************************!*\
  !*** ./src/graphql/types/character.graphql ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n    var doc = {\"kind\":\"Document\",\"definitions\":[{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Character\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"fullName\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"movies\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"ListType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Movie\"}}}},\"directives\":[]}]},{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Query\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"character\"},\"arguments\":[{\"kind\":\"InputValueDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]}],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Character\"}}},\"directives\":[]}]}],\"loc\":{\"start\":0,\"end\":133}};\n    doc.loc.source = {\"body\":\"type Character {\\n    id: String!\\n    fullName: String!\\n    movies: [Movie]!\\n}\\n\\ntype Query {\\n    character(id: String!): Character!\\n}\\n\",\"name\":\"GraphQL request\",\"locationOffset\":{\"line\":1,\"column\":1}};\n  \n\n    var names = {};\n    function unique(defs) {\n      return defs.filter(\n        function(def) {\n          if (def.kind !== 'FragmentDefinition') return true;\n          var name = def.name.value\n          if (names[name]) {\n            return false;\n          } else {\n            names[name] = true;\n            return true;\n          }\n        }\n      )\n    }\n  \n\n      module.exports = doc;\n    \n\n\n//# sourceURL=webpack:///./src/graphql/types/character.graphql?");

/***/ }),

/***/ "./src/graphql/types/course.graphql":
/*!******************************************!*\
  !*** ./src/graphql/types/course.graphql ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n    var doc = {\"kind\":\"Document\",\"definitions\":[{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Course\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"ID\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"fullname\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"shortname\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"idnumber\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"summary\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"startdate\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Int\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"enddate\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Int\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"visible\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Boolean\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"fullnamedisplay\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"viewurl\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"courseimage\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"progress\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Int\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"hasprogress\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Boolean\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"isfavourite\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Boolean\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"hidden\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Boolean\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"timeaccess\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Int\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"showshortname\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Boolean\"}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"coursecategory\"},\"arguments\":[],\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}},\"directives\":[]}]},{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Query\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"courses\"},\"arguments\":[],\"type\":{\"kind\":\"ListType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Course\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"course\"},\"arguments\":[{\"kind\":\"InputValueDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]}],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Course\"}}},\"directives\":[]}]}],\"loc\":{\"start\":0,\"end\":445}};\n    doc.loc.source = {\"body\":\"type Course {\\n  id: ID!\\n  fullname:  String,\\n  shortname: String,\\n  idnumber: String,\\n  summary: String,\\n  startdate: Int,\\n  enddate: Int\\n  visible: Boolean,\\n  fullnamedisplay:  String\\n  viewurl: String\\n  courseimage: String\\n  progress: Int\\n  hasprogress: Boolean,\\n  isfavourite: Boolean,\\n  hidden: Boolean,\\n  timeaccess: Int\\n  showshortname: Boolean\\n  coursecategory: String\\n}\\n\\ntype Query {\\n  courses: [Course]\\n  course(id: String!): Course!\\n}\\n\",\"name\":\"GraphQL request\",\"locationOffset\":{\"line\":1,\"column\":1}};\n  \n\n    var names = {};\n    function unique(defs) {\n      return defs.filter(\n        function(def) {\n          if (def.kind !== 'FragmentDefinition') return true;\n          var name = def.name.value\n          if (names[name]) {\n            return false;\n          } else {\n            names[name] = true;\n            return true;\n          }\n        }\n      )\n    }\n  \n\n      module.exports = doc;\n    \n\n\n//# sourceURL=webpack:///./src/graphql/types/course.graphql?");

/***/ }),

/***/ "./src/graphql/types/index.js":
/*!************************************!*\
  !*** ./src/graphql/types/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! merge-graphql-schemas */ \"merge-graphql-schemas\");\n/* harmony import */ var merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _movie_graphql__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movie.graphql */ \"./src/graphql/types/movie.graphql\");\n/* harmony import */ var _movie_graphql__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_movie_graphql__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _character_graphql__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./character.graphql */ \"./src/graphql/types/character.graphql\");\n/* harmony import */ var _character_graphql__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_character_graphql__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _course_graphql__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./course.graphql */ \"./src/graphql/types/course.graphql\");\n/* harmony import */ var _course_graphql__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_course_graphql__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(merge_graphql_schemas__WEBPACK_IMPORTED_MODULE_0__[\"mergeTypes\"])([_movie_graphql__WEBPACK_IMPORTED_MODULE_1___default.a, _character_graphql__WEBPACK_IMPORTED_MODULE_2___default.a, _course_graphql__WEBPACK_IMPORTED_MODULE_3___default.a], {\n  all: true\n}));\n\n//# sourceURL=webpack:///./src/graphql/types/index.js?");

/***/ }),

/***/ "./src/graphql/types/movie.graphql":
/*!*****************************************!*\
  !*** ./src/graphql/types/movie.graphql ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n    var doc = {\"kind\":\"Document\",\"definitions\":[{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Movie\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"title\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"characters\"},\"arguments\":[],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"ListType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Character\"}}}},\"directives\":[]}]},{\"kind\":\"ObjectTypeDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"Query\"},\"interfaces\":[],\"directives\":[],\"fields\":[{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"movies\"},\"arguments\":[],\"type\":{\"kind\":\"ListType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Movie\"}}},\"directives\":[]},{\"kind\":\"FieldDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"movie\"},\"arguments\":[{\"kind\":\"InputValueDefinition\",\"name\":{\"kind\":\"Name\",\"value\":\"id\"},\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"String\"}}},\"directives\":[]}],\"type\":{\"kind\":\"NonNullType\",\"type\":{\"kind\":\"NamedType\",\"name\":{\"kind\":\"Name\",\"value\":\"Movie\"}}},\"directives\":[]}]}],\"loc\":{\"start\":0,\"end\":136}};\n    doc.loc.source = {\"body\":\"type Movie {\\n  id: String!\\n  title: String!\\n  characters: [Character]!\\n}\\n\\ntype Query {\\n  movies: [Movie]\\n  movie(id: String!): Movie!\\n}\\n\",\"name\":\"GraphQL request\",\"locationOffset\":{\"line\":1,\"column\":1}};\n  \n\n    var names = {};\n    function unique(defs) {\n      return defs.filter(\n        function(def) {\n          if (def.kind !== 'FragmentDefinition') return true;\n          var name = def.name.value\n          if (names[name]) {\n            return false;\n          } else {\n            names[name] = true;\n            return true;\n          }\n        }\n      )\n    }\n  \n\n      module.exports = doc;\n    \n\n\n//# sourceURL=webpack:///./src/graphql/types/movie.graphql?");

/***/ }),

/***/ "./src/handler.js":
/*!************************!*\
  !*** ./src/handler.js ***!
  \************************/
/*! exports provided: main */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ \"./src/index.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"main\", function() { return _index_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]; });\n\n\n\n//# sourceURL=webpack:///./src/handler.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var apollo_server_lambda__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! apollo-server-lambda */ \"apollo-server-lambda\");\n/* harmony import */ var apollo_server_lambda__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(apollo_server_lambda__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _graphql_resolvers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./graphql/resolvers */ \"./src/graphql/resolvers/index.js\");\n/* harmony import */ var _graphql_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./graphql/types */ \"./src/graphql/types/index.js\");\n/* harmony import */ var _graphql_dataSources_character__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./graphql/dataSources/character */ \"./src/graphql/dataSources/character.js\");\n/* harmony import */ var _graphql_dataSources_movie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./graphql/dataSources/movie */ \"./src/graphql/dataSources/movie.js\");\n/* harmony import */ var _graphql_dataSources_course__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./graphql/dataSources/course */ \"./src/graphql/dataSources/course.js\");\n\n\n\n\n\n // creating the server\n\nconst server = new apollo_server_lambda__WEBPACK_IMPORTED_MODULE_0__[\"ApolloServer\"]({\n  // passing types and resolvers to the server\n  typeDefs: _graphql_types__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n  resolvers: _graphql_resolvers__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  // initial context state, will be available in resolvers\n  context: () => ({}),\n  // an object that goes to the \"context\" argument\n  // when executing resolvers\n  dataSources: () => {\n    return {\n      characterSource: _graphql_dataSources_character__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\n      movieSource: _graphql_dataSources_movie__WEBPACK_IMPORTED_MODULE_4__[\"default\"],\n      courseSource: _graphql_dataSources_course__WEBPACK_IMPORTED_MODULE_5__[\"default\"]\n    };\n  }\n});\n\nconst handler = (event, context, callback) => {\n  const handler = server.createHandler(); // tell AWS lambda we do not want to wait for NodeJS event loop\n  // to be empty in order to send the response\n\n  context.callbackWaitsForEmptyEventLoop = false; // process the request\n\n  return handler(event, context, callback);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (handler);\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "apollo-server-lambda":
/*!***************************************!*\
  !*** external "apollo-server-lambda" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"apollo-server-lambda\");\n\n//# sourceURL=webpack:///external_%22apollo-server-lambda%22?");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"axios\");\n\n//# sourceURL=webpack:///external_%22axios%22?");

/***/ }),

/***/ "merge-graphql-schemas":
/*!****************************************!*\
  !*** external "merge-graphql-schemas" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"merge-graphql-schemas\");\n\n//# sourceURL=webpack:///external_%22merge-graphql-schemas%22?");

/***/ })

/******/ })));